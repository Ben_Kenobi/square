﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

[System.Serializable]
public class Dialog
{
     public List<Answer> answers = new List<Answer>();
     public List<Message> messages = new List<Message>();   
}

[System.Serializable]
public class Message
{
    public int id = 0;
    public string speaker = "";
    public string icon = "";
    public string text = "";
    public string img = "";
    public string video = "";
    public string sound = "";
    public int goToDial = 0;
}

[System.Serializable]
public class Answer
{
    public int id = 0;
    public string speaker = "";
    public string icon = "";
    public string text = "";

    public List<Choice> choices;
}

[System.Serializable]
public class Choice
{
    public string content = "";
    public int goToDial = 0;
}

[System.Serializable]
public class TchatMessage
{
    public Text textContainer;
    public Image imgContainer;
    public VideoPlayer videoPlayer;
    public Image bg;
    public Image avatar;

    public int nexDialId;
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Video;

[Serializable]
public class TchatManager : MonoBehaviour
{
    public GameObject tchatPanel;

    //Message component
    [Header("Message elements")]
    public GameObject messageTemplate;
    public GameObject answerTemplate;

    [Header("Answers Panel")]
    public GameObject choicesPanel;
    public GameObject choiceButton;
    public List<GameObject> btns;

    [Header("Gif list")]
    public List<GameObject> gifList;

    [Header("Message List")]
    [SerializeField]
    List<TchatMessage> messageList = new List<TchatMessage>();
    [SerializeField]
    private string[] dialboxes;//used to know what is the next or current dialog type (paco or player ?)
    public Dialog dialog;

    [Header("File")]
    public string fileName = "marche";

    public void SendMessageToTchat(Message msg)
    {
        TchatMessage newMessage = SetupMsgUI(msg);
        messageList.Add(newMessage);

        if (msg.goToDial != 0)
        {
            LoadNextMessage(msg.goToDial);
        }
    }

    public void SendAnswerToTchat(Choice choice)
    {
        //remove choices button
        foreach(GameObject btn in btns)
        {
            Destroy(btn);
        }

        //send message from player in tchat
        TchatMessage newAnswer = SetupAnswerUI(choice);
        messageList.Add(newAnswer);

        LoadNextMessage(choice.goToDial);
    }

    public void PrepareAnswerAndChoices(Answer ans)
    {
        foreach (Choice choice in ans.choices)
        {
            //duplicate choice button //add it to the choice panel
            GameObject newChoiceButton = Instantiate(choiceButton, choicesPanel.transform);
            btns.Add(newChoiceButton);

            //set the button text base on choice content
            newChoiceButton.GetComponentInChildren<Text>().text = choice.content;

            //add click listener to the button
            newChoiceButton.GetComponent<Button>().onClick.AddListener(delegate { SendAnswerToTchat(choice); });
        }
    }

    public TchatMessage SetupMsgUI(Message msg)
    {
        //dupplicate template message object
        GameObject msgObj = Instantiate(messageTemplate, tchatPanel.transform);
        msgObj.transform.SetAsLastSibling(); // set as last element of the list to continue scrolling to bottom in the scrollview

        TchatMessage newMessage = new TchatMessage();

        if(msg.text != "")
        {
            newMessage.textContainer = msgObj.gameObject.GetComponentInChildren<Text>();
            newMessage.textContainer.text = msg.text;
        }
        else
        {
            msgObj.gameObject.GetComponentInChildren<Text>().enabled = false;
        }

        if(msg.video != "")
        {
            newMessage.videoPlayer = msgObj.gameObject.GetComponentInChildren<VideoPlayer>();
        }

        SetupPicturesInTchatMessage(newMessage, msg, msgObj);
       

        return newMessage;
    }

    public TchatMessage SetupAnswerUI(Choice choice)
    {
        GameObject ansObj = Instantiate(answerTemplate, tchatPanel.transform);
        ansObj.transform.SetAsLastSibling();     

        TchatMessage newAnswer = new TchatMessage();

        if(choice.content != "")
        {
            newAnswer.textContainer = ansObj.GetComponentInChildren<Text>();
            newAnswer.textContainer.text = choice.content;
        }
        else
        {
            ansObj.gameObject.GetComponentInChildren<Text>().enabled = false;
        }

        newAnswer.nexDialId = choice.goToDial;

        return newAnswer;
    }

    private void SetupPicturesInTchatMessage(TchatMessage messageInTchat, Message msg, GameObject msgObj)
    {
        Image[] containers = msgObj.GetComponentsInChildren<Image>();

        foreach (Image currentContainer in containers)
        {
            switch (currentContainer.name)
            {
                case "Avatar":
                    SetMessageAvatarImage(messageInTchat, currentContainer, msg);

                    break;

                case "Picture":
                    SetMessageArtworkImage(messageInTchat, currentContainer, msg, msgObj);

                    break;
            }
        }       
    }

    void SetMessageAvatarImage(TchatMessage currentTchatMsg, Image container, Message msg)
    {
        currentTchatMsg.avatar = container;
        Sprite avatar;

        if (msg.icon == "paco")
        {
            avatar = Resources.Load<Sprite>("Tchat/Images/paco");
        }
        else
        {
            avatar = Resources.Load<Sprite>("Tchat/Images/player");
        }

        currentTchatMsg.avatar.sprite = avatar;
    }

    void SetMessageArtworkImage(TchatMessage currentTchatMsg, Image container, Message msg, GameObject msgObj)
    {
        if (msg.img != null && msg.img != "" && msg.img.Contains("."))
        {

            string extension = Path.GetExtension(msg.img);

            currentTchatMsg.imgContainer = container;
            
            string fileName = msg.img.Substring(0, msg.img.LastIndexOf("."));
            Sprite image = Resources.Load<Sprite>("Tchat/Images/" + fileName);

            //that's not a true gif, only a trick to be able to set animation image another way
            if(extension == ".gif")
            {
                //desactivate current image container, cause animation not setted the same way and need animator and animation
                container.gameObject.SetActive(false);

                //check in the list of the gif prefabs to instantiate the good one*
                foreach(GameObject gifPrefab in gifList)
                {
                    if(gifPrefab.name.ToLower() == fileName.ToLower())
                    {
                        GameObject gif = Instantiate(gifPrefab, msgObj.transform.GetChild(0).transform);
                        gif.transform.SetAsFirstSibling();
                    }
                }

                return;
            }

            if (image != null)
            {
                currentTchatMsg.imgContainer.sprite = image;
                return;
            }    
            
            container.gameObject.SetActive(false);
            return;
        }
        
        container.gameObject.SetActive(false);
       
    }

    void SetupDialog(Dialog dialog)
    {
        int messageNumbers = dialog.messages.Count;
        int answerNumbers = dialog.answers.Count;

        dialboxes = new string[messageNumbers + answerNumbers];

        foreach (Message msg in dialog.messages)
        {
            dialboxes[msg.id] = "message";
        }

        foreach (Answer ans in dialog.answers)
        {
            dialboxes[ans.id] = "answer";
        }

        if (dialboxes[0] == "message")
        {
            SendMessageToTchat(dialog.messages[0]);
            return;
        }

        PrepareAnswerAndChoices(dialog.answers[0]);
    }

    void ReadDialogFile(string fileName)
    {
        try
        {
            string dialogFile = Resources.Load<TextAsset>("Tchat/Texts/" + fileName).ToString();
            dialog = JsonUtility.FromJson<Dialog>(dialogFile);            

            SetupDialog(dialog);
        }
        catch(Exception e)
        {
            Debug.LogError("Error loading json file");
            Debug.LogError(e);
        }
    }

    void LoadNextMessage(int nextId)
    {

        if(nextId == 9999)
        {
            Debug.Log("dialog end");
            return;
        }
        //First need to display a gif to let player know someone is typing
        //get next message type and send it
        string next = dialboxes[nextId];

        if(next == "message")
        {
            for(int i = 0; i < dialog.messages.Count; i++)
            {
                if(dialog.messages[i].id == nextId)
                {
                    SendMessageToTchat(dialog.messages[i]);
                }
            }
        }
        else
        {
            for (int j = 0; j < dialog.answers.Count; j++)
            {
                if (dialog.answers[j].id == nextId)
                {
                    PrepareAnswerAndChoices(dialog.answers[j]);
                }
            }
        }

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ReadDialogFile(fileName);
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            Message debugMsgTemplate = new Message();
            debugMsgTemplate.text = "test lecture auto gif";
            SendMessageToTchat(debugMsgTemplate);
        }
    }
}

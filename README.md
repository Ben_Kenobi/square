# Prérequis

- installer Unity version 2019.1.1f1 (AVEC LES OPTIONS POUR ANDROID)
- avoir un téléphone Android (minimum Android 4.1)
- Le mettre en mode développeur et activer le débogage USB
- installer le Android SDK (ou vérifier qu'il est bien présent à l'installation d'Unity -> je conseille l'intsllation via le Unity HUB)
    -> Aller dans Edit -> Preferences  pour vérifier les JDK, SDK et NDK disponible 

# Lancer le projet

- cloner le repo pour avoir les dernières updates (branche Master par défaut)
- Vérifier dans File -> Build settings que la plateforme choisie est bien Android
- si besoin, la sélectionner puis cliquer sur "switch platform"
- (attendre que les compilations s'éxecutent)
- Vérifier que la scène (dans le gros encart en haut "Scenes in build") est bien celle que l'on veut tester, si besoin les modifier ("Add Open Scenes")
- Build and Run

---

# Le Tchat
## Rajouter un gif

Nos gifs sont gérés dans les messages de la scène Chat. On ne peut pas les utiliser avec un "Sprite Renderer" pour les utiliser, car l'animation se trouvera dasn un cavans, donc des éléments d'UI.

1. Télécharger un gif qui nous plait
2. Aller sur ezgif.com ( https://ezgif.com/split )
3. Dans l'onglet split, uploader le gif et le télécharger sous forme de séquence PNG (split options -> ouput images in PNG format) (faire super attention au poids final)
4. Créer un nouveau dossier dans le projet unity dans Assets/Images/Tchat/gifs/ aussi bien dans "animated" que "sequencies" portant le nom du gif (un nom cohérent sans espaces et caractères spéciaux)
5. Drag'n drop toutes les images de la séquences PNG dans le dossier fréchement créé dans "sequencies"
6. Dans unity, Sélectionner toutes les images et dans l'onglet de "Inspector", les transformer en sous l'onglet Texture Type "Sprite 2D UI" et cliquer en bas sur "apply"
7. Séléctionner toutes les images dans unity et les glisser dans l'arborescence de la scène
8. Unity crée un nouveau fichier ```.anim``` automatiquement, qu'il faut enregistrer dans le dossier du nom du gif, dans /animated
9. Renommer le nom de l'objet dans l'arborescence de la scène par le nom du gif
10. Aller ouvrir le fichier d'animation (le ```.anim``` dans /animated) et le renommer au préalable si besoin
11. Sélectionner l'objet qui correspond à notre gif dans la scène et setté le nombre de samples pour que celui-ci soit lu à une vitesse de lecture correcte.
12. En fonction de l'endroit où l'on souhaite placer l'animation, il faut créer une nouvelle "UI Image" et choisir dans la source la première frame de l'animation. Il faut ensuite ajouter un composant "animator" et aller chercher comme controller, l'anomator controller qu'unity a créé automatiquement (dans /animated) 
13. Pour plus de facilité, on peut ensuite transformé le tout en "prefab", en venant drag'n'drop l'élément animé de la hiérarchie de la scène dans le dossier /animated. On pourra ensuite l'utiliser à la voler sans avoir à paramétrer quoi que ce soit en plus.
14. Ne pas oublier de les mettres dans la liste des gifs disponible en cilquadn sur l'élément TchatManager dans la hierarchie, et en venant placer le nouveau Prefab dans la liste

